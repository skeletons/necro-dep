# Necro Dependency Generator 

Necro Dep is a dependency generator for Skel files. Given a file, it computes
the dependencies between the declared values and generates a graph summarizing
them.

Several options will allow you to work on this graph and print it. Its current
version is far from definitive.

## Installation

### Via `opam`

```
opam switch create necro 4.09.1
eval $(opam env)
opam repository add necro https://gitlab.inria.fr/skeletons/opam-repository.git#necro
opam install -y necrodep
```

### From the sources

#### Dependencies

* bash
* autoconf
* OCaml (4.09 or greater)
* menhir
* make
* necrolib (> 3.0.0)

To install from the sources, you should first have
[necrolib](https://gitlab.inria.fr/skeletons/necro) installed on your computer,
from the sources also.

Then type `autoconf; ./configure; make`. It should ask for `necrolib`'s path.

## How-to use it

NB: We will assume that `necrodep` is in your `PATH`. If it is not, prefix it by
its path.

Running `necrodep file.sk` will compute the dependency graph and print a list of
all roots, that is all values which are never called by any other values.

Running `necrodep file.sk -o file.dot` will compute the dependency graph and
print it in the
[`dot`](https://en.wikipedia.org/wiki/DOT_(graph_description_language)) format
in the file `file.dot`. The specified values are reprezented with ellipses, and
the non-specified one with rectangles.

Options will probably be added, but if you have any idea as to what should be
implemented, don't hesitate to pull request or to submit a feature request as an
issue.
