open Necro
open Skeltypes
open Interpretation
open Graph
open Util

type spec = | Spec | Unspec

let dependencies_interpretation: (SSet.t, SSet.t) interpretation' =
	{ var_interp' = (fun x ->
			begin match x with
			| TVLet _ -> SSet.empty
			| TVValue ([], s, _, _) ->
					SSet.singleton s
			| _ -> SSet.empty (* LATER: make a graph for several files *)
			end)
	; constr_interp' = (fun _ _ t -> t)
	; tuple_interp' = (fun tl -> sset_list_union tl)
	; function_interp' = (fun _ _ s -> s)
	; field_interp' = (fun t _ _ -> t)
	; record_interp' = (fun t tl ->
				SSet.union (Option.value ~default:SSet.empty t)
				(sset_list_union (List.map (fun (_, _, x) -> x) tl)))
	; letin_interp' = (fun b _ s1 s2 ->
				SSet.union (Option.fold ~none:SSet.empty ~some:(fun (f, s, tl) ->
						if f <> [] then SSet.empty (* LATER: make a graph for several files? *)
						else SSet.singleton s.contents) b)
				(SSet.union s1 s2))
	; exists_interp' = (fun _ _ s -> s)
	; apply_interp' = (fun t tl -> sset_list_union (t :: tl))
	; merge_interp' = (fun _ bl -> sset_list_union bl)
	; return_interp' = (fun s -> s)
	}

let dependencies (name, (_, _, _, v)) =
	let deps, spec =
		begin match v with
		| None -> SSet.empty, Unspec
		| Some v -> interpret_value' dependencies_interpretation v.contents, Spec
		end
	in
	name, deps, spec

let create_graph sem values =
	let deps = List.map dependencies values in
	let g = Graph.empty in
	let g =
		List.fold_left (fun g (v, _, spec) ->
			Graph.add_vertex g v spec) g deps
	in
	let g =
		List.fold_left (fun g (v, vdeps, _) ->
			SSet.fold (fun w g ->
				Graph.add_edge g v w) vdeps g) g deps
	in
	g


let log name g ff =
	let roots = SMap.bindings (Graph.roots g) in
	(* Print all roots *)
	Format.fprintf ff "@[<v 2>The roots for file %s are:@,%a@]" name
	(Format.pp_print_list (fun ff (x,spec) ->
		let spec_annot = if spec = Unspec then " (non-specified)" else "" in
		Format.fprintf ff "- %s%s" x spec_annot)) roots

let generate_log sem name =
	let values = SMap.bindings sem.ss_values in
	let g = create_graph sem values in
	let ff = Format.str_formatter in
	let () = log name g ff in
	Format.flush_str_formatter ()



(*
 *let print_value ff (name, (_, _, _, v)) =
 *  begin match dependencies v with
 *  | [] -> Format.fprintf ff "\"%s\";" name
 *  | l ->
 *      Format.pp_print_list (fun ff x ->
 *        Format.fprintf ff "\"%s\" -> \"%s\";" name x) ff l
 *  end
 *)
let print_vertex g ff (x, spec) =
	let s = Graph.next g x in
	begin match SSet.is_empty s with
	| true ->
			let spec_annot = if spec = Unspec then "" else " [shape = rectangle]" in
			Format.fprintf ff "\"%s\"%s" x spec_annot
	| false ->
			Format.fprintf ff "\"%s\" -> {%a}" x (Format.pp_print_list
			~pp_sep:(fun ff () -> Format.fprintf ff " ") (fun ff s -> Format.fprintf
			ff "\"%s\"" s)) (SSet.elements s)
	end

let print_graph ff name g =
	Format.fprintf ff "@[<v 2>digraph %s {@,%a@]@,}"
	name (Format.pp_print_list (print_vertex g)) (SMap.bindings @@ Graph.vertices g)

let generate_dot sem name =
	let values = SMap.bindings sem.ss_values in
	let g = create_graph sem values in
	let ff = Format.str_formatter in
	let () = print_graph ff name g in
	Format.flush_str_formatter ()

