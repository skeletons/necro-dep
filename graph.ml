open Necro
open Util

type 'a t =
	{ vertices: 'a SMap.t
	; edges: SSet.t SMap.t
	; retro_edges: SSet.t SMap.t
	}

let empty =
	{ vertices = SMap.empty
	; edges = SMap.empty
	; retro_edges = SMap.empty
	}

let next g v =
	SMap.find v g.edges

let prev g v =
	SMap.find v g.retro_edges

let add_vertex g v attr =
	if SMap.mem v g.vertices then g else
	let vertices = SMap.add v attr g.vertices in
	let edges = SMap.add v SSet.empty g.edges in
	let retro_edges = SMap.add v SSet.empty g.retro_edges in
	{ vertices ; edges ; retro_edges }

let add_edge g x y =
	let n = next g x in
	let n' = SSet.add y n in
	let edges = SMap.add x n' g.edges in
	let p = prev g y in
	let p' = SSet.add x p in
	let retro_edges = SMap.add y p' g.retro_edges in
	{ g with edges ; retro_edges }

let add_edge_attr g x attrx y attry =
	let g = add_vertex g x attrx in
	let g = add_vertex g y attry in
	add_edge g x y

let is_root g x =
	SSet.subset (SMap.find x g.retro_edges) (SSet.singleton x)

let roots g =
	SMap.filter (fun name attr -> is_root g name) g.vertices

let get_attr g x = SMap.find x g.vertices

let set_attr g x a = {g with vertices = SMap.add x a g.vertices}

let vertices g = g.vertices

let subgraph g s =
	let vertices = SMap.filter (fun x _ -> SSet.mem x s) g.vertices in
	let fix edges =
		let edges1 =
			SMap.filter (fun x _ -> SSet.mem x s) edges
		in
		let edges2 =
			SMap.map (fun x -> SSet.inter s x) edges1
		in
		edges2
	in
	{ vertices
	; edges = fix g.edges
	; retro_edges = fix g.retro_edges
	}

