open Necro
open Util

type 'a t
val empty: 'a t
val next: 'a t -> string -> SSet.t
val prev: 'a t -> string -> SSet.t
val add_vertex: 'a t -> string -> 'a -> 'a t
val add_edge: 'a t -> string -> string -> 'a t
val add_edge_attr: 'a t -> string -> 'a -> string -> 'a -> 'a t
val roots: 'a t -> 'a SMap.t
val set_attr: 'a t -> string -> 'a -> 'a t
val get_attr: 'a t -> string -> 'a
val vertices: 'a t -> 'a SMap.t
val subgraph: 'a t -> SSet.t -> 'a t
